# This Docker file downloads the DB2 LUW binaries and installs them in the
# image. It installs the necessary libraries.
#
# This Docker is designed to download a fixpack from IBM and to install DB2
# from it. The fixpack is from the server binaries that correspond to the
# Version: 2018-05.09
# Author: Bo Thurgren
# Made in Sweden
 
FROM centos:7
 
LABEL maintainer="bo.thurgren@gmail.com"
ENV DB2_VERSION 11.1
ENV TERMINAL vt100
## Directory of the installers. Associated to the edition.
ENV DB2_INST_DIR expc
## Name of the response file for the installation.
ENV DB2_RESP_FILE_INSTALL db2_${DB2_VERSION}_express.rsp
ENV DB2_RESP_FILE db2_${DB2_VERSION}_makeinstance.rsp
 
## Script to create the instance
ENV DB2_INST_CREA_SCR createInstance.sh
 
## Name of the downloaded file.
ENV DB2_INSTALLER v${DB2_VERSION}_linuxx64_${DB2_INST_DIR}.tar.gz
 
ARG DB2INST_PWD
# Copies the script to /tmp
COPY ${DB2_INSTALLER} /tmp
COPY ${DB2_RESP_FILE_INSTALL} /tmp
COPY ${DB2_RESP_FILE} /tmp
COPY ${DB2_INST_CREA_SCR} /tmp
COPY make_install.sh /tmp
COPY after_install.sh /tmp
COPY update_dbm_cfg.sql /tmp
COPY update_db_cfg.sql /tmp
COPY start-db.sh /tmp
 
# Run all commands in a single RUN layer to minimize the size of the image.
RUN printf "Running install on pam , ncurses-libs.i686, file, curl, libaio,numactl, libstdc\n"  \
	&& yum install -y \
    pam \
    pam.i686 \
    ncurses-libs.i686 \
    file \
    curl \
    libaio \
    numactl \
    libstdc++-devel.i686 >/dev/null \
    && yum clean all
 
RUN groupadd -g 55501 db2iadm1 \
    && useradd -u 55501 -d /home/db2inst1 -g db2iadm1 -m -s /bin/bash -p ${DB2INST_PWD} db2inst1
 
RUN cd /tmp \
	&& printf "Untar of ${DB2_INSTALLER}\n" \
    && tar -zvxf ${DB2_INSTALLER} >/dev/null \
    && chmod 750 /tmp/make_install.sh /tmp/after_install.sh /tmp/${DB2_INST_CREA_SCR} /tmp/start-db.sh \
    && /tmp/make_install.sh \
    && mkdir -p /db2data/db2inst1/db2diag \
    && mkdir -p /db2data/db2inst1/db2logs \
    && chown -R db2inst1:db2iadm1 /db2data/db2inst1 /tmp/update_db_cfg.sql /tmp/start-db.sh \
    && /tmp/${DB2_INST_CREA_SCR} \
    && /tmp/after_install.sh \
    && rm -rf  /tmp/${DB2_INST_DIR} \
    && rm -f /tmp/${DB2_RESP_FILE_INSTALL} \
    && rm -f /tmp/${DB2_INSTALLER} \
    && rm -f /tmp/${DB2_INST_CREA_SCR}
 
EXPOSE 50000