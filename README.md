### db2-docker/README.md at master · bothurgren/db2_express · GitHub
 
Historically and unlike other database vendors, IBM produced a platform-specific DB2 product for each of its major operating systems.
However, in the 1990s IBM changed track and produced a DB2 "common server" product, designed with a common code base to run on different platforms
 
#### wikipedia.org/wiki/IBM_DB2
#### DB2 LUW
#### DB2 Express-C
A DB2 instance is a running process that controls the security at the higher level, listens on a specific port,
controls the databases it hosts, and many other operations related to database administration.
 
This image will download and install DB2 LUW Express-C, but it will not create an instance nor a database.
 
For the DB2 installation, a provided response file is used. You can clone this repository and modify the response file for your own needs.
 
DB2 Express-C will be installed in the container in:
/opt/ibm/db2/V11.1
This image has a set scripts to ease the instance creation. The script that creates the instance follows the instructions of a response file.
The instance by default is db2inst1 listening on port 50000 installed in the /home/db2inst1 directory.
 
The build process can be done via a pull or directly from the sources via build. This will create the image that will be ready to run.
 
#### Clone from sources
git clone https://bothurgren@bitbucket.org/bothurgren/db2_express.git
 
cd db2_express
#### docker build
image_repo=db2-express-centos;
image_tag=latest;
 
docker build --build-arg DB2INST_PWD=mySecretPwd -t ${image_repo}:${image_tag} .
 

#### Set name variables
instname=db2inst1;
port=50000;
insthome=/home/${instname};
contname=${image_repo};
datavol=/db2data;
 
#### Create volumes
docker volume create ${contname}_db2data

#### Run container
docker run --privileged=true -p $port:$port --mount source=${contname}_db2data,destination=$datavol -t -d -u $instname --name="${contname}" ${image_repo}:${image_tag} /bin/su -c '${insthome}/sqllib/adm/db2start;/tmp/start-db.sh' - $instname
 
#### Copy ddls scripts
docker cp DDLSCRIPT.ddl ${image_repo}:/tmp
 
#### docker exec
docker exec -u root ${image_repo} su -c ". ~db2inst1/sqllib/db2profile ;db2 -td@ -vf /tmp/DDLSCRIPT.ddl" db2inst1
 
Once the instance has been created, you can run the DB2 instance as a Docker daemon.
 
Once the container with DB2 is running, it can be used to create, populate, manipulate and perform other activities with databases.
 
If you want to access the console, you need to do an attach to the container
sudo attach db2inst1
 