#!/bin/bash

# Helper to create an instance, start it and change to it.
# This script could receive a parameter for the instance name.
#
# Version: 2018-05-12
# Author: Bo Thurgren

RESP_FILE=db2_11.1_makeinstance.rsp
# Creates the instance according to the response file.
echo "Creating instance..."
/opt/ibm/db2/V11.1/instance/db2isetup -r /tmp/${RESP_FILE}
